package br.com.nubank.domain;

/**
 * Created by douglas on 6/30/15.
 */
public interface UseCase {
    void sendToPresenter();
    void execute();

}
