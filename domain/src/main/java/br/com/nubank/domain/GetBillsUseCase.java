package br.com.nubank.domain;

import br.com.nubank.model.responses.BillsApiResponse;

/**
 * Created by douglas on 6/30/15.
 */
public interface GetBillsUseCase extends UseCase {
    void getBills();
    void onBillsReceived(BillsApiResponse response);
}
