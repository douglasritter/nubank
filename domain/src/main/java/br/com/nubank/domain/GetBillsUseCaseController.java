package br.com.nubank.domain;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import br.com.nubank.common.utils.BusProvider;
import br.com.nubank.model.DataSource;
import br.com.nubank.model.responses.BillsApiResponse;

/**
 * Created by douglas on 7/13/15.
 */
public class GetBillsUseCaseController implements GetBillsUseCase {
    private final DataSource mDataSource;
    private final Bus mUiBus;

    private BillsApiResponse mBillsResponse;

    public GetBillsUseCaseController(DataSource dataSource, Bus uiBus) {
        if (dataSource == null || uiBus == null)
            throw new IllegalArgumentException("DataSource and Bus cannot be null");

        mDataSource = dataSource;
        mUiBus = uiBus;

        BusProvider.getRestBusInstance().register(this);

    }


    @Override public void getBills() {
        mDataSource.getBills();
    }

    @Subscribe
    @Override public void onBillsReceived(BillsApiResponse response) {
        mBillsResponse = response;
        sendToPresenter();
    }

    @Override public void sendToPresenter() {
        mUiBus.post(mBillsResponse);
        BusProvider.getRestBusInstance().unregister(this);
    }

    @Override public void execute() {
        getBills();
    }


}
