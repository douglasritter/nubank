package br.com.nubank.android.views.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.Space;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.nubank.android.R;
import br.com.nubank.android.mvp.presenters.BillsPresenter;
import br.com.nubank.android.mvp.views.BillsView;
import br.com.nubank.android.views.adapters.BillsPagerAdapter;
import br.com.nubank.common.utils.BusProvider;
import br.com.nubank.model.entities.Bill;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class BillsActivity extends AppCompatActivity implements BillsView {

    // Constants
    public static final String LOG_TAG = BillsActivity.class.getSimpleName();

    // Java Objects
    private BillsPresenter mBillsPresenter;
    private BillsPagerAdapter mBillsAdapter;

    // UI Objects
    @Bind(R.id.pager) protected ViewPager mViewPager;
    @Bind(R.id.scrollView) protected HorizontalScrollView mScrollView;
    @Bind(R.id.tabsContainer) protected LinearLayout mTabsContainer;
    @Bind(R.id.testButton) protected Button mButton;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills);
        ButterKnife.bind(this);
        mBillsPresenter = new BillsPresenter(this);
        BusProvider.getUIBusInstance().register(this);

    }

    public void populateBar(List<Bill> billsList){
        Space space = new Space(getContext());
        space.setLayoutParams(new ViewGroup.LayoutParams(270, ViewGroup.LayoutParams.MATCH_PARENT));
        mTabsContainer.addView(space);

        for(int i=0; i<billsList.size(); i++){
            Bill bill = billsList.get(i);

            TextView textView = new TextView(this);
            textView.setText(new SimpleDateFormat("MMM").format(bill.getSummary().getDueDate().getTime()).toUpperCase());
            textView.setGravity(Gravity.CENTER);
            textView.setTextSize(getResources().getInteger(R.integer.month_size));
            textView.setTextColor(getResources().getColor(getColorId(bill)));
            textView.setLayoutParams(new ViewGroup.LayoutParams(200, ViewGroup.LayoutParams.MATCH_PARENT));
            mTabsContainer.addView(textView);
        }

        Space spaceEnd = new Space(getContext());
        spaceEnd.setLayoutParams(new ViewGroup.LayoutParams(270, ViewGroup.LayoutParams.MATCH_PARENT));
        mTabsContainer.addView(spaceEnd);

    }

    @OnClick(R.id.testButton) public void moveViewToCenter(){
        int indexSelected = 3;
        int widthSum = 0;
        for(int i=0; i<indexSelected+1; i++){
            widthSum += mTabsContainer.getChildAt(i).getLayoutParams().width;
            Log.e("WIDTH SUM", ""+widthSum);
        }
        widthSum -= mTabsContainer.getChildAt(indexSelected+1).getLayoutParams().width / 3;
        mScrollView.scrollTo(widthSum/2, 0);

    }


    @Override
    protected void onStart() {
        super.onStart();
        mBillsAdapter = new BillsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mBillsAdapter);
        getData();
    }

    public void getData(){
        mBillsPresenter.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mBillsPresenter.stop();
    }

    @Override public void showBills(List<Bill> billsList) {
        Log.e(LOG_TAG, "bills list size: "+ billsList.size());
        mBillsAdapter.setBills(billsList);
        populateBar(billsList);
        Log.e(LOG_TAG, "date from hell "+billsList.get(0).getLineItems().get(0).getPostDate().getTime());

    }

    @Override public void showLoading() {

    }

    @Override public void hideLoading() {

    }

    @Override public void showError(String error) {
        Log.e(LOG_TAG, "Error: "+ error);
    }

    @Override public void hideError() {

    }

    @Override public Context getContext() {
        return this;
    }

    public static int getColorId(Bill bill){
        switch (bill.getState()){
            case OVERDUE:
                return R.color.bill_overdue;
            case CLOSED:
                return R.color.bill_closed;
            case FUTURE:
                return R.color.bill_future;
            default:
                return R.color.bill_open;
        }
    }
}
