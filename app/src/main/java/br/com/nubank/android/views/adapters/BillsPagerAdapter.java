package br.com.nubank.android.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import br.com.nubank.android.views.fragments.BillFragment;
import br.com.nubank.model.entities.Bill;

/**
 * Created by douglas on 7/13/15.
    */
    public class BillsPagerAdapter extends FragmentStatePagerAdapter {

        public static final String LOG_TAG = "BillsPagerAdapter";

        List<Bill> mBills;

        public BillsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setBills(List<Bill> bills){
            this.mBills = bills;
            notifyDataSetChanged();
        }

        @Override public int getCount() {
            int count = mBills != null ? mBills.size() : 0;
            return count;
        }

    @Override public Fragment getItem(int position) {
        return BillFragment.newInstance(mBills.get(position));
    }


}
