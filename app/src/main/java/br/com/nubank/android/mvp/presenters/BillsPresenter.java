package br.com.nubank.android.mvp.presenters;

import android.util.Log;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import br.com.nubank.android.mvp.views.BillsView;
import br.com.nubank.common.utils.BusProvider;
import br.com.nubank.domain.GetBillsUseCaseController;
import br.com.nubank.domain.UseCase;
import br.com.nubank.model.entities.Bill;
import br.com.nubank.model.responses.BillsApiResponse;
import br.com.nubank.model.rest.AppUrls;
import br.com.nubank.model.rest.RestSource;
import retrofit.RetrofitError;

/**
 * Created by douglas on 7/13/15.
 */
public class BillsPresenter extends Presenter {

    private final BillsView mBillsView;

    public BillsPresenter(BillsView billsView) {
        mBillsView = billsView;
    }

    @Subscribe public void onBillsReceived(BillsApiResponse response) {
        List<Bill> mBills = new ArrayList<>();
        mBills.addAll(response.getBills());
        mBillsView.showBills(mBills);
    }

    @Subscribe public void onError(RetrofitError error) {
        mBillsView.showError(error.getMessage());
    }

    @Override public void start() {
        try {
            BusProvider.getUIBusInstance().register(this);
            UseCase getBills = new GetBillsUseCaseController(RestSource.getInstance(AppUrls.DEFAULT_NAMESPACE), BusProvider.getUIBusInstance());
            getBills.execute();
        } catch (IllegalArgumentException e) {
            Log.e(BillsPresenter.class.getSimpleName(), e.getMessage(), e);
        }
    }

    public void refresh() {
        UseCase getBills = new GetBillsUseCaseController(RestSource.getInstance(AppUrls.DEFAULT_NAMESPACE), BusProvider.getUIBusInstance());
        getBills.execute();
    }

    @Override public void stop() {
        try {
            BusProvider.getUIBusInstance().unregister(this);
        } catch (IllegalArgumentException e) {
            Log.e(BillsPresenter.class.getSimpleName(), e.getMessage(), e);
        }
    }

}
