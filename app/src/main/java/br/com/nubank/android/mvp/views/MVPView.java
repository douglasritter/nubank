package br.com.nubank.android.mvp.views;

/**
 * Root class for view classes
 */
public interface MVPView {
    public android.content.Context getContext();
}
