package br.com.nubank.android.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.nubank.android.R;
import br.com.nubank.android.views.activities.BillsActivity;
import br.com.nubank.common.utils.CurrencyUtils;
import br.com.nubank.model.entities.Bill;
import br.com.nubank.model.entities.BillState;
import butterknife.Bind;
import butterknife.ButterKnife;

public class BillFragment extends Fragment {

    // Constants
    public static final String LOG_TAG = BillFragment.class.getSimpleName();
    public static final String BILL_TAG = "bill_parcelable_tag";

    // Java Objects
    private LinearLayoutManager mLayoutManager;
    private Bill mBill;

    //UI Objects
    @Bind(R.id.shotsList) protected RecyclerView mLinesList;
    @Bind(R.id.billValueInternContainer) protected LinearLayout mBillValueInternContainer;
    @Bind(R.id.billDueValue) protected TextView mBillDueValue;
    @Bind(R.id.billDueDate) protected TextView mBillDueDate;
    @Bind(R.id.billDetailsMonthTotalContent) protected TextView mBillDetailsMonthTotalContent;
    @Bind(R.id.billDetailsNotPaidContent) protected TextView mBillDetailsNotPaidContent;
    @Bind(R.id.billDetailsInterestContent) protected TextView mBillDetailsInterestContent;
    @Bind(R.id.billDetailsGenerateContainer) protected RelativeLayout mBillDetailsGenerateContainer;
    @Bind(R.id.billDetailsGenerateButton) protected Button mBillDetailsGenerateButton;

    public static BillFragment newInstance(Bill bill) {
        BillFragment fragment = new BillFragment();
        Bundle args = new Bundle();
        args.putParcelable(BILL_TAG, bill);
        fragment.setArguments(args);

        return fragment;
    }

    public BillFragment() {}

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBill = getArguments().getParcelable(BILL_TAG);
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bill, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Populate RecyclerView
//        mShotsAdapter = new ShotsAdapter(this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLinesList.setLayoutManager(mLayoutManager);
//        mLinesList.setAdapter(mShotsAdapter);

        if(mBill != null){
            mBillValueInternContainer.setBackgroundColor(getResources().getColor(BillsActivity.getColorId(mBill)));
            mBillDueValue.setText(CurrencyUtils.getFormatted(mBill.getSummary().getTotalBalance()));
            String formattedDate = (String) android.text.format.DateFormat.format("dd MMM", mBill.getSummary().getDueDate());
            mBillDueDate.setText(getResources().getString(R.string.bill_due_date_text)+ " "+formattedDate.toUpperCase());

            mBillDetailsMonthTotalContent.setText(CurrencyUtils.getFormatted(mBill.getSummary().getTotalCumulative()));
            mBillDetailsNotPaidContent.setText(CurrencyUtils.getFormatted(mBill.getSummary().getTotalBalance()));
            mBillDetailsInterestContent.setText(CurrencyUtils.getFormatted(mBill.getSummary().getInterest()));

            if(mBill.getLinks() != null){
                if(mBill.getState() == BillState.OVERDUE){
                    mBillDetailsGenerateButton.setBackgroundResource(R.drawable.button_generate_over);
                    mBillDetailsGenerateButton.setTextColor(getResources().getColor(R.color.bill_overdue));
                    mBillDetailsGenerateContainer.setVisibility(View.VISIBLE);
                } else if(mBill.getState() == BillState.OPEN){
                    mBillDetailsGenerateButton.setBackgroundResource(R.drawable.button_generate_open);
                    mBillDetailsGenerateButton.setTextColor(getResources().getColor(R.color.bill_open));
                    mBillDetailsGenerateContainer.setVisibility(View.VISIBLE);
                }
            }

        }

    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
