package br.com.nubank.android.mvp.views;

import java.util.List;

import br.com.nubank.model.entities.Bill;

/**
 * Created by douglas on 7/13/15.
 */
public interface BillsView extends MVPView {
    void showBills(List<Bill> billsList);
    void showLoading();
    void hideLoading();
    void showError(String error);
    void hideError();

}
