package br.com.nubank.model.rest;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import br.com.nubank.common.utils.BusProvider;
import br.com.nubank.model.BillDeserializerJson;
import br.com.nubank.model.DataSource;
import br.com.nubank.model.entities.BillsArray;
import br.com.nubank.model.responses.BillsApiResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class RestSource implements DataSource {
    public static RestSource INSTANCE;
    private OkHttpClient mClient;
    private final NubankAPI mNubankAPI;

    private RestSource(String namespace) {
        mClient = new OkHttpClient();

        GsonConverter converter = new GsonConverter(new GsonBuilder()
                .registerTypeAdapter(BillsArray.class, new BillDeserializerJson())
                .create());

        RestAdapter nubankAPIRest = new RestAdapter.Builder()
                .setEndpoint(namespace)
                .setConverter(converter)
                .build();

        nubankAPIRest.setLogLevel(RestAdapter.LogLevel.FULL);
        mNubankAPI = nubankAPIRest.create(NubankAPI.class);
    }

    public static RestSource getInstance(String namespace) {
        if (INSTANCE == null)
            INSTANCE = new RestSource(namespace);
        return INSTANCE;
    }

    @Override public void getBills() {
        mNubankAPI.getBills(retrofitCallback);
    }

    public Callback retrofitCallback = new Callback<BillsArray>() {
        @Override
        public void success(BillsArray billsArray, Response response) {
            BillsApiResponse billsApiResponse = new BillsApiResponse();
            billsApiResponse.setBills(billsArray.getBillList());
            BusProvider.getRestBusInstance().post(billsApiResponse);
        }

        @Override public void failure(RetrofitError error) {
            BusProvider.getUIBusInstance().post(error);
        }
    };

}