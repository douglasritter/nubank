package br.com.nubank.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by douglas on 7/5/15.
 */
public class Summary implements Parcelable {

    @SerializedName("due_date") private String dueDate;
    @SerializedName("close_date") private String closeDate;
    @SerializedName("post_balance") private double pastBalance;
    @SerializedName("total_balance") private double totalBalance;
    private double interest;
    @SerializedName("total_cumulative") private double totalCumulative;
    private double paid;
    @SerializedName("minimum_payment") private double minimumPayment;
    @SerializedName("open_date") private String openDate;

    public Summary() {}

    public Date getDueDate() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(dueDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Date getCloseDate() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(closeDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public double getPastBalance() {
        return pastBalance;
    }

    public void setPastBalance(double pastBalance) {
        this.pastBalance = pastBalance;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getTotalCumulative() {
        return totalCumulative;
    }

    public void setTotalCumulative(double totalCumulative) {
        this.totalCumulative = totalCumulative;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public double getMinimumPayment() {
        return minimumPayment;
    }

    public void setMinimumPayment(double minimumPayment) {
        this.minimumPayment = minimumPayment;
    }

    public Date getOpenDate() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(openDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dueDate);
        dest.writeString(this.closeDate);
        dest.writeDouble(this.pastBalance);
        dest.writeDouble(this.totalBalance);
        dest.writeDouble(this.interest);
        dest.writeDouble(this.totalCumulative);
        dest.writeDouble(this.paid);
        dest.writeDouble(this.minimumPayment);
        dest.writeString(this.openDate);
    }

    protected Summary(Parcel in) {
        this.dueDate = in.readString();
        this.closeDate = in.readString();
        this.pastBalance = in.readDouble();
        this.totalBalance = in.readDouble();
        this.interest = in.readDouble();
        this.totalCumulative = in.readDouble();
        this.paid = in.readDouble();
        this.minimumPayment = in.readDouble();
        this.openDate = in.readString();
    }

    public static final Parcelable.Creator<Summary> CREATOR = new Parcelable.Creator<Summary>() {
        public Summary createFromParcel(Parcel source) {
            return new Summary(source);
        }

        public Summary[] newArray(int size) {
            return new Summary[size];
        }
    };
}
