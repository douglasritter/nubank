package br.com.nubank.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by douglas on 7/5/15.
 */
public class Links implements Parcelable {

    private Link self;
    @SerializedName("boleto_email") private Link emailBillet;
    private Link barcode;

    public Links() {}

    public Link getSelf() {
        return self;
    }

    public void setSelf(Link self) {
        this.self = self;
    }

    public Link getEmailBillet() {
        return emailBillet;
    }

    public void setEmailBillet(Link emailBillet) {
        this.emailBillet = emailBillet;
    }

    public Link getBarcode() {
        return barcode;
    }

    public void setBarcode(Link barcode) {
        this.barcode = barcode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.self, flags);
        dest.writeParcelable(this.emailBillet, flags);
        dest.writeParcelable(this.barcode, flags);
    }

    protected Links(Parcel in) {
        this.self = in.readParcelable(Link.class.getClassLoader());
        this.emailBillet = in.readParcelable(Link.class.getClassLoader());
        this.barcode = in.readParcelable(Link.class.getClassLoader());
    }

    public static final Parcelable.Creator<Links> CREATOR = new Parcelable.Creator<Links>() {
        public Links createFromParcel(Parcel source) {
            return new Links(source);
        }

        public Links[] newArray(int size) {
            return new Links[size];
        }
    };
}
