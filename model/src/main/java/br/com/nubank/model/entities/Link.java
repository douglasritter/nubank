package br.com.nubank.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Link implements Parcelable {
    private String href;

    public Link() {}

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.href);
    }

    protected Link(Parcel in) {
        this.href = in.readString();
    }

    public static final Creator<Link> CREATOR = new Creator<Link>() {
        public Link createFromParcel(Parcel source) {
            return new Link(source);
        }

        public Link[] newArray(int size) {
            return new Link[size];
        }
    };
}