package br.com.nubank.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import br.com.nubank.model.entities.Bill;
import br.com.nubank.model.entities.BillsArray;

public class BillDeserializerJson implements JsonDeserializer<BillsArray> {

    @Override
    public BillsArray deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {
        BillsArray billsArray = new BillsArray();
        JsonArray jsonArray = je.getAsJsonArray();
        Gson gson = new Gson();
        for(JsonElement element : jsonArray){
            JsonObject jsonObject = element.getAsJsonObject();
            Bill bill = gson.fromJson(jsonObject.get("bill"), Bill.class);
            billsArray.add(bill);
        }
        return billsArray;

    }
}