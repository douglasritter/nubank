package br.com.nubank.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by douglas on 7/5/15.
 */
public class LineItem implements Parcelable {

    @SerializedName("post_date") private String postDate;
    private double amount;
    private String title;
    private double index;
    private double charges;
    private String href;

    public LineItem() {}

    public Date getPostDate() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(postDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getIndex() {
        return index;
    }

    public void setIndex(double index) {
        this.index = index;
    }

    public double getCharges() {
        return charges;
    }

    public void setCharges(double charges) {
        this.charges = charges;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.postDate);
        dest.writeDouble(this.amount);
        dest.writeString(this.title);
        dest.writeDouble(this.index);
        dest.writeDouble(this.charges);
        dest.writeString(this.href);
    }

    protected LineItem(Parcel in) {
        this.postDate = in.readString();
        this.amount = in.readDouble();
        this.title = in.readString();
        this.index = in.readDouble();
        this.charges = in.readDouble();
        this.href = in.readString();
    }

    public static final Parcelable.Creator<LineItem> CREATOR = new Parcelable.Creator<LineItem>() {
        public LineItem createFromParcel(Parcel source) {
            return new LineItem(source);
        }

        public LineItem[] newArray(int size) {
            return new LineItem[size];
        }
    };
}
