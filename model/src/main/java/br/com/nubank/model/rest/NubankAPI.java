package br.com.nubank.model.rest;

import br.com.nubank.model.entities.BillsArray;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by douglas on 7/5/15.
 */
public interface NubankAPI {

    //https://s3-sa-east-1.amazonaws.com/mobile-challenge/bill/bill_new.json
    @GET("/mobile-challenge/bill/bill_new.json")
    void getBills(Callback<BillsArray> callback);

}
