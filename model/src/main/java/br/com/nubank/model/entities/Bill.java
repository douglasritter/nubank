package br.com.nubank.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by douglas on 7/5/15.
 */
public class Bill implements Parcelable {

  private BillState state;
  private String id;
  private Summary summary;
  @SerializedName("_links") private Links links;
  private String barcode;
  @SerializedName("linha_digitavel") private String typeableLine;
  @SerializedName("line_items") private List<LineItem> lineItems;

  public Bill() {}

  public BillState getState() {
    return state;
  }

  public void setState(BillState state) {
    this.state = state;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Summary getSummary() {
    return summary;
  }

  public void setSummary(Summary summary) {
    this.summary = summary;
  }

  public Links getLinks() {
    return links;
  }

  public void setLinks(Links links) {
    this.links = links;
  }

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public String getTypeableLine() {
    return typeableLine;
  }

  public void setTypeableLine(String typeableLine) {
    this.typeableLine = typeableLine;
  }

  public List<LineItem> getLineItems() {
    return lineItems;
  }

  public void setLineItems(List<LineItem> lineItems) {
    this.lineItems = lineItems;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.state == null ? -1 : this.state.ordinal());
    dest.writeString(this.id);
    dest.writeParcelable(this.summary, flags);
    dest.writeParcelable(this.links, flags);
    dest.writeString(this.barcode);
    dest.writeString(this.typeableLine);
    dest.writeList(this.lineItems);
  }

  protected Bill(Parcel in) {
    int tmpState = in.readInt();
    this.state = tmpState == -1 ? null : BillState.values()[tmpState];
    this.id = in.readString();
    this.summary = in.readParcelable(Summary.class.getClassLoader());
    this.links = in.readParcelable(Links.class.getClassLoader());
    this.barcode = in.readString();
    this.typeableLine = in.readString();
    this.lineItems = new ArrayList<LineItem>();
    in.readList(this.lineItems, List.class.getClassLoader());
  }

  public static final Parcelable.Creator<Bill> CREATOR = new Parcelable.Creator<Bill>() {
    public Bill createFromParcel(Parcel source) {
      return new Bill(source);
    }

    public Bill[] newArray(int size) {
      return new Bill[size];
    }
  };
}
