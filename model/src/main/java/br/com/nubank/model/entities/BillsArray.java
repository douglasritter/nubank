package br.com.nubank.model.entities;

import java.util.LinkedList;
import java.util.List;

public class BillsArray {
    List<Bill> billList;

    public List<Bill> getBillList() {
        return billList;
    }

    public void add(Bill billItem){
        if(billList == null){
            billList = new LinkedList<Bill>();
        }
        billList.add(billItem);
    }
}