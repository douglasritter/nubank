package br.com.nubank.model.responses;

import java.util.List;

import br.com.nubank.model.entities.Bill;

/**
 * Created by douglas on 7/5/15.
 */
public class BillsApiResponse extends ApiReponse{

    private List<Bill> bills;

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }
}
