package br.com.nubank.model.entities;

import com.google.gson.annotations.SerializedName;

public enum BillState {
    @SerializedName("overdue")
    OVERDUE ("overdue"),
    @SerializedName("closed")
    CLOSED ("closed"),
    @SerializedName("future")
    FUTURE ("future"),
    @SerializedName("open")
    OPEN ("open");

    private final String name;       

    private BillState(String s) {
        name = s;
    }

    public boolean equalsName(String otherStatus){
        return (otherStatus == null) ? false : name.equals(otherStatus);
    }

    public String toString(){
       return name;
    }

}