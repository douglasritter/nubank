package br.com.nubank.common.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by douglas on 7/14/15.
 */
public class CurrencyUtils {

    public static String getFormatted(double value){
        Locale locale = new Locale("pt", "BR");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
        return formatter.format(value/100).replaceAll("R\\$", "R\\$ ");
    }


}
